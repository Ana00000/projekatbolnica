/***********************************************************************
 * Module:  FunctionOfRoom.cs
 * Author:  Asus
 * Purpose: Definition of the Class Model.ManagerModel.FunctionOfRoom
 ***********************************************************************/


namespace Model.ManagerModel
{
    public enum FunctionOfRoom
    {
        OperationRoom,
        ControlRoom,
        RoomForLaying,
        Storage
    }
}