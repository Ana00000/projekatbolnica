/***********************************************************************
 * Module:  Gender.cs
 * Author:  sladj
 * Purpose: Definition of the Class Model.UserModel.Gender
 ***********************************************************************/

using System;

namespace Model.UserModel
{
   public enum Gender
   {
      Male,
      Female
   
   }
}