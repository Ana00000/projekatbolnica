/***********************************************************************
 * Module:  TypeOfUser.cs
 * Author:  sladj
 * Purpose: Definition of the Class Model.UserModel.TypeOfUser
 ***********************************************************************/

using System;

namespace Model.UserModel
{
   public enum TypeOfUser
   {
      Manager,
      Secretary,
      Doctor,
      Patient,
      Specialist
   }
}