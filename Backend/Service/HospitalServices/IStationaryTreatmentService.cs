/***********************************************************************
 * Module:  IStationaryTreatmentService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.HospitalServices.IStationaryTreatmentService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IStationaryTreatmentService : IService
   {
      void HospitalizePatient(Patient patient, Model.ManagerModel.Bed bed);
      void DischargePatient(Patient patient);
      void RelocatePatient(Patient patient, Model.ManagerModel.Bed bed);
      Model.UtilityModel.TimePeriod DurationOfLayingInBed(Model.ManagerModel.Bed bed);
   }
}