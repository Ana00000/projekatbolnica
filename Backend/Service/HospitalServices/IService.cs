/***********************************************************************
 * Module:  IService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.HospitalServices.IService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IService
   {
      void Set(Object obj);
      Object Get(Object obj);
      List<Object> GetAll();
      void New(Object obj);
      void Delete(Object obj);
   }
}