/***********************************************************************
 * Module:  IActivitySevice.cs
 * Author:  Jelena
 * Purpose: Definition of the Interface Service.HospitalServices.IActivitySevice
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IActivitySevice : IService
   {
      Boolean IsValidAppointment(Model.DoctorModel.Appointment appointment);
      Boolean IsAvailableDoctor(Model.Doctor doctor, DateTime term);
      Boolean IsAvailableTerm(DateTime term, List<Object> obj);
      Boolean IsAvailableRoom(Model.ManagerModel.Room room, DateTime term);
   }
}