/***********************************************************************
 * Module:  IApprovalOfMedicineService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.HospitalServices.IApprovalOfMedicineService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IApprovalOfMedicineService
   {
      void ApproveMedicine(Model.ManagerModel.Medicine medicine);
      void DisapproveMedicine(Model.ManagerModel.Medicine medicine);
      List<Medicine> SearchMedicine(String query);
   }
}