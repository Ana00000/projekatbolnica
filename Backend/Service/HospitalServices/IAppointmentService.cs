/***********************************************************************
 * Module:  IAppointmentService.cs
 * Author:  Jelena
 * Purpose: Definition of the Interface Service.HospitalServices.IAppointmentService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IAppointmentService : IActivitySevice
   {
      int CountAppointmentsInRoom(Model.ManagerModel.Room room, Model.UtilityModel.TimePeriod timePeriod);
      Model.DoctorModel.Appointment RecommendAppointmentByDoctor(Doctor doctor, DateTime date);
      Model.DoctorModel.Appointment RecommendAppointmentByDoctor2(DateTime date);
   }
}