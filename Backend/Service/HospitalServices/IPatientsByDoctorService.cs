/***********************************************************************
 * Module:  IPatientsByDoctorService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.HospitalServices.IPatientsByDoctorService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IPatientsByDoctorService : IService
   {
      Boolean IsAllergicTo(Patient patient, Model.ManagerModel.Medicine medicine);
      void AddMedicalRecord(Patient patient, Model.PatientModel.MedicalRecord record);
      Model.DoctorModel.DoctorReport GenerateReport(Patient patient, Model.UtilityModel.TimePeriod timePeriod);
      void ReferToSpecialist(Patient patient, Model.DoctorModel.Specialist specialist);
      List<Patient> SearchPatients(String query);
   }
}