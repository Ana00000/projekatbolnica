/***********************************************************************
 * Module:  IOperationService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.HospitalServices.IOperationService
 ***********************************************************************/

using System;

namespace Service.HospitalServices
{
   public interface IOperationService : IActivitySevice
   {
      int CountOperationsInRoom(Model.ManagerModel.Room room, Model.UtilityModel.TimePeriod timePeriod);
   }
}