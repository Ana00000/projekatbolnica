/***********************************************************************
 * Module:  IUserService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.UserService.IUserService
 ***********************************************************************/

using System;

namespace Service.UserService
{
   public interface IUserService : Service.HospitalServices.IService
   {
      String AddFeedback(String message);
      Model.UserModel.Language ChangeLanguage(Model.UserModel.Language language);
      void Registration(User user);
      void Login(String username, String password, Boolean stayLoggedIn);
      Boolean IsUsernameValid(String username);
      Boolean IsPasswordValid(String password);
   }
}