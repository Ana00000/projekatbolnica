/***********************************************************************
 * Module:  IIngredientService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IIngredientService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IIngredientService : Service.HospitalServices.IService
   {
      Boolean IsNameValid(String name);
   }
}