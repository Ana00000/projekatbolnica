/***********************************************************************
 * Module:  RenovationOfRoomsService.cs
 * Author:  Asus
 * Purpose: Definition of the Class Service.ManagerService.RenovationOfRoomsService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public class RenovationOfRoomsService : IRenovationOfRoomsService
   {
      public Model.ManagerModel.RenovationOfRooms AnnounceAction(Model.UtilityModel.TimePeriod from, Model.UtilityModel.TimePeriod to, Model.ManagerModel.Room room)
      {
         // TODO: implement
         return null;
      }
      
      public Model.ManagerModel.Room RelocateResources(List<Resource> resources, Model.ManagerModel.Room room)
      {
         // TODO: implement
         return null;
      }
      
      public Model.ManagerModel.FunctionOfRoom ChangeFunctionOfRoom(Model.ManagerModel.FunctionOfRoom functionOfRoom)
      {
         // TODO: implement
         return null;
      }
      
      public String ChangeNameOfRoom(String nameOfRoom)
      {
         // TODO: implement
         return null;
      }
   
      public Repository.ManagerRepository.IRoomsRepository iRoomsRepository;
   
   }
}