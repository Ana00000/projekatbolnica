/***********************************************************************
 * Module:  IMedicineService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IMedicineService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IMedicineService : Service.HospitalServices.IService
   {
      void SendApprovalRequest(List<Doctor> doctors);
      Boolean IsApprovalAccepted(Model.ManagerModel.Medicine medicine);
      Boolean IsNameValid(String name);
   }
}