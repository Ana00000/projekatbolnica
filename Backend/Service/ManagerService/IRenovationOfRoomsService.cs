/***********************************************************************
 * Module:  IRenovationOfRoomsService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IRenovationOfRoomsService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IRenovationOfRoomsService
   {
      Model.ManagerModel.RenovationOfRooms AnnounceAction(Model.UtilityModel.TimePeriod from, Model.UtilityModel.TimePeriod to, Model.ManagerModel.Room room);
      Model.ManagerModel.Room RelocateResources(List<Resource> resources, Model.ManagerModel.Room room);
      Model.ManagerModel.FunctionOfRoom ChangeFunctionOfRoom(Model.ManagerModel.FunctionOfRoom functionOfRoom);
      String ChangeNameOfRoom(String nameOfRoom);
   }
}