/***********************************************************************
 * Module:  IResourceService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IResourceService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IResourceService : Service.HospitalServices.IService
   {
      void IncreaseQuantity(String id, int quantity);
      void DecreaseQuantity(String id, int quantity);
      int GetAmountOfResource(int id);
      Boolean IsNameValid(String name);
   }
}