/***********************************************************************
 * Module:  IRoomService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IRoomService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IRoomService : Service.HospitalServices.IActivitySevice, Service.HospitalServices.IService
   {
      int CountNumberOfBedsInRoom(Model.ManagerModel.Room room);
      Boolean IsNameValid(String name);
   }
}