/***********************************************************************
 * Module:  ResourceService.cs
 * Author:  Asus
 * Purpose: Definition of the Class Service.ManagerService.ResourceService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public class ResourceService : IResourceService
   {
      public void IncreaseQuantity(String id, int quantity)
      {
         // TODO: implement
      }
      
      public void DecreaseQuantity(String id, int quantity)
      {
         // TODO: implement
      }
      
      public int GetAmountOfResource(int id)
      {
         // TODO: implement
         return 0;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Repository.ManagerRepository.IMedicineRepository iMedicineRepository;
      public Repository.ManagerRepository.IResourceRepository iResourceRepository;
   
   }
}