/***********************************************************************
 * Module:  IWorkHoursForDoctorService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.ManagerService.IWorkHoursForDoctorService
 ***********************************************************************/

using System;

namespace Service.ManagerService
{
   public interface IWorkHoursForDoctorService : Service.HospitalServices.IService
   {
      int CalculateNumberOfDoctorsOnDuty(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours);
      int CalculateNumberOfDoctorsOnVacation(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours);
      Double CalculateOvertimeWorkInPercantage(int overtimeAnnualWork, int overtimeWeeklyWork);
      List<Doctor> OccupationOfDoctors(Model.UtilityModel.TimePeriod from, Model.UtilityModel.TimePeriod to, List<Doctor> doctors);
   }
}