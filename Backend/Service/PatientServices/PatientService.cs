/***********************************************************************
 * Module:  PatientService.cs
 * Author:  Asus
 * Purpose: Definition of the Class Service.PatientServices.PatientService
 ***********************************************************************/

using System;

namespace Service.PatientServices
{
   public class PatientService : IPatientService
   {
      public List<Appointment> ViewAppointments(DateTime beginDate, DateTime endDate)
      {
         // TODO: implement
         return null;
      }
      
      public List<MedicalRecord> ViewRecords(DateTime beginDate, DateTime endDate)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey StartSurvey(Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.MedicalRecord AddMedicalRecord(Model.PatientModel.MedicalRecord medicalRecord)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Repository.PatientRepository.IPatientRepository iPatientRepository;
   
   }
}