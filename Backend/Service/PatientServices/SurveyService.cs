/***********************************************************************
 * Module:  SurveyService.cs
 * Author:  Asus
 * Purpose: Definition of the Class Service.PatientServices.SurveyService
 ***********************************************************************/

using System;

namespace Service.PatientServices
{
   public class SurveyService : ISurveyService
   {
      public Model.PatientModel.QuestionsAndAnswers AddQuestionAndAnswerToSurvey(string question)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey ChangeSurveryDescription(string description, Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey ChangeSurveryName(string newName, Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Repository.PatientRepository.ISurveryRepository iSurveryRepository;
   
   }
}