/***********************************************************************
 * Module:  IPatientService.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Service.PatientServices.IPatientService
 ***********************************************************************/

using System;

namespace Service.PatientServices
{
   public interface IPatientService : Service.HospitalServices.IService
   {
      List<Appointment> ViewAppointments(DateTime beginDate, DateTime endDate);
      List<MedicalRecord> ViewRecords(DateTime beginDate, DateTime endDate);
      Model.PatientModel.Survey StartSurvey(Model.PatientModel.Survey survey);
      Model.PatientModel.MedicalRecord AddMedicalRecord(Model.PatientModel.MedicalRecord medicalRecord);
   }
}