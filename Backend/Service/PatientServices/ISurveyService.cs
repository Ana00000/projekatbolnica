/***********************************************************************
 * Module:  ISurveyService.cs
 * Author:  sladj
 * Purpose: Definition of the Interface Service.PatientServices.ISurveyService
 ***********************************************************************/

using System;

namespace Service.PatientServices
{
   public interface ISurveyService : Service.HospitalServices.IService
   {
      Model.PatientModel.QuestionsAndAnswers AddQuestionAndAnswerToSurvey(string question);
      Model.PatientModel.Survey ChangeSurveryDescription(string description, Model.PatientModel.Survey survey);
      Model.PatientModel.Survey ChangeSurveryName(string newName, Model.PatientModel.Survey survey);
   }
}