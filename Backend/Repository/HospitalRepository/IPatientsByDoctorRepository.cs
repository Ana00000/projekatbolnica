/***********************************************************************
 * Module:  IPatientsByDoctorRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.HospitalRepository.IPatientsByDoctorRepository
 ***********************************************************************/

using System;

namespace Repository.HospitalRepository
{
   public interface IPatientsByDoctorRepository : IRepository
   {
      List<Patient> GetPatientsByDoctor(Doctor doctor);
   }
}