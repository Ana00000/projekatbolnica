/***********************************************************************
 * Module:  Interface1.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.Interface1
 ***********************************************************************/

using System;

namespace Repository.HospitalRepository
{
   public interface IRepository
   {
      void Set(Object obj);
      Object Get(Object obj);
      List<Object> GetAll();
      void New(Object obj);
      void Delete(Object obj);
      void OpenFile();
      void CloseFile();
   }
}