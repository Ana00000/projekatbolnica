/***********************************************************************
 * Module:  IActivity.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.HospitalRepository.IActivity
 ***********************************************************************/

using System;

namespace Repository.HospitalRepository
{
   public interface IActivity : IRepository
   {
      List<Object> GetActivityByDoctor(Doctor doctor);
      List<Object> GetActivityByDate(DateTime date);
      List<Object> GetActivityByPatient(Patient patient);
      List<Object> GetActivityByRoom(Model.ManagerModel.Room room);
   }
}