/***********************************************************************
 * Module:  IStationaryTreatmentRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.HospitalRepository.IStationaryTreatmentRepository
 ***********************************************************************/

using System;

namespace Repository.HospitalRepository
{
   public interface IStationaryTreatmentRepository : IRepository
   {
      void RelocatePatient(Patient patient, Model.ManagerModel.Bed bed);
   }
}