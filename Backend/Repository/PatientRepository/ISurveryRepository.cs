/***********************************************************************
 * Module:  ISurveryRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.PatientRepository.ISurveryRepository
 ***********************************************************************/

using System;

namespace Repository.PatientRepository
{
   public interface ISurveryRepository : Repository.HospitalRepository.IRepository
   {
   }
}