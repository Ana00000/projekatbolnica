/***********************************************************************
 * Module:  IMedicalRecord.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.UserRepository.IMedicalRecord
 ***********************************************************************/

using System;

namespace Repository.PatientRepository
{
   public interface IMedicalRecordRepository : Repository.HospitalRepository.IRepository
   {
   }
}