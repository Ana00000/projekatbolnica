/***********************************************************************
 * Module:  IPatientRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.PatientRepository.IPatientRepository
 ***********************************************************************/

using System;

namespace Repository.PatientRepository
{
   public interface IPatientRepository : Repository.HospitalRepository.IRepository
   {
   }
}