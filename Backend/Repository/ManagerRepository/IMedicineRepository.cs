/***********************************************************************
 * Module:  IMedicineRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.IMedicineRepository
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface IMedicineRepository : Repository.HospitalRepository.IRepository
   {
      Model.ManagerModel.Medicine GetMedicineByName(String name);
      List<Medicine> GetAllUnapprovedMedicine();
      void AddInUnapprovedList(Model.ManagerModel.Medicine medicine);
   }
}