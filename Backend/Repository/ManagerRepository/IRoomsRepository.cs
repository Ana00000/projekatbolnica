/***********************************************************************
 * Module:  IRoomsRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.IRoomsRepository
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface IRoomsRepository : Repository.HospitalRepository.IRepository
   {
      Model.ManagerModel.Room GetRoomById(int id);
   }
}