/***********************************************************************
 * Module:  IIngredientRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.IIngredientRepository
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface IIngredientRepository : Repository.HospitalRepository.IRepository
   {
   }
}