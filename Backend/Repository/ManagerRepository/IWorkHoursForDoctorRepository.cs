/***********************************************************************
 * Module:  IWorkHoursForDoctorRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.IWorkHoursForDoctorRepository
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface IWorkHoursForDoctorRepository : Repository.HospitalRepository.IRepository
   {
   }
}