/***********************************************************************
 * Module:  IResourceRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.IResourceRepository
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface IResourceRepository : Repository.HospitalRepository.IRepository
   {
   }
}