/***********************************************************************
 * Module:  Interface1.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.ManagerRepository.Interface1
 ***********************************************************************/

using System;

namespace Repository.ManagerRepository
{
   public interface Interface1
   {
      void Set(Model.ManagerModel.Resource resource);
      Object Get(String id);
      List<Object> GetAll();
      void New(Model.ManagerModel.Resource resource);
      void Delete(Model.ManagerModel.Resource resource);
   }
}