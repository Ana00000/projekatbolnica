/***********************************************************************
 * Module:  INotificationRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.UserRepository.INotificationRepository
 ***********************************************************************/

using System;

namespace Repository.UserRepository
{
   public interface INotificationRepository : Repository.HospitalRepository.IRepository
   {
   }
}