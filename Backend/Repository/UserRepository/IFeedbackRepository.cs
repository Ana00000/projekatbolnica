/***********************************************************************
 * Module:  IFeedbackRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.UserRepository.IFeedbackRepository
 ***********************************************************************/

using System;

namespace Repository.UserRepository
{
   public interface IFeedbackRepository : Repository.HospitalRepository.IRepository
   {
   }
}