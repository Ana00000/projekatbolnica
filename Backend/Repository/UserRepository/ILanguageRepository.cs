/***********************************************************************
 * Module:  ILanguageRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.UserRepository.ILanguageRepository
 ***********************************************************************/

using System;

namespace Repository.UserRepository
{
   public interface ILanguageRepository : Repository.HospitalRepository.IRepository
   {
   }
}