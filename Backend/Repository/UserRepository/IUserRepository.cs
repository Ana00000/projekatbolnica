/***********************************************************************
 * Module:  IUserRepository.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Repository.UserRepository.IUserRepository
 ***********************************************************************/

using System;

namespace Repository.UserRepository
{
   public interface IUserRepository : Repository.HospitalRepository.IRepository
   {
      List<Doctor> GetAllDoctors();
      List<Secretary> GetAllSecretaries();
      List<Patient> GetAllPatients();
   }
}