/***********************************************************************
 * Module:  AppointmentService.cs
 * Author:  Jelena
 * Purpose: Definition of the Class Service.SecretaryService.AppointmentService
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public class AppointmentController : IAppointmentController
   {
      public int CountAppointmentsInRoom(Model.ManagerModel.Room room, Model.UtilityModel.TimePeriod timePeriod)
      {
         // TODO: implement
         return 0;
      }
      
      public Model.DoctorModel.Appointment RecommendAppointmentByDoctor(Doctor doctor, DateTime date)
      {
         // TODO: implement
         return null;
      }
      
      public Model.DoctorModel.Appointment RecommendAppointmentByDoctor2(DateTime date)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.HospitalServices.IAppointmentService iAppointmentService;
   
   }
}