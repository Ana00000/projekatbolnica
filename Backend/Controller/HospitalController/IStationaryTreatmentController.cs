/***********************************************************************
 * Module:  IStationaryTreatmentController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.HospitalController.IStationaryTreatmentController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IStationaryTreatmentController : IController
   {
      void HospitalizePatient(Patient patient, Model.ManagerModel.Bed bed);
      void DischargePatient(Patient patient);
      void RelocatePatient(Patient patient, Model.ManagerModel.Bed bed);
      Model.UtilityModel.TimePeriod DurationOfLayingInBed(Model.ManagerModel.Bed bed);
   }
}