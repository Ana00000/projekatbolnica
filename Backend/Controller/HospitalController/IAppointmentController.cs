/***********************************************************************
 * Module:  IAppointmentController.cs
 * Author:  sladj
 * Purpose: Definition of the Interface Controller.HospitalController.IAppointmentController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IAppointmentController : IController
   {
      int CountAppointmentsInRoom(Model.ManagerModel.Room room, Model.UtilityModel.TimePeriod timePeriod);
      Model.DoctorModel.Appointment RecommendAppointmentByDoctor(Doctor doctor, DateTime date);
      Model.DoctorModel.Appointment RecommendAppointmentByDoctor2(DateTime date);
   }
}