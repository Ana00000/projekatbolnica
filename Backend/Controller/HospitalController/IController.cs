/***********************************************************************
 * Module:  IController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.UserController.IController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IController
   {
      void Set(Object obj);
      Object Get(Object obj);
      List<Object> GetAll();
      void New(Object obj);
      void Delete(Object obj);
   }
}