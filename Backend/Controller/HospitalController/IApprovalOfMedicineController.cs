/***********************************************************************
 * Module:  IApprovalOfMedicineController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.HospitalController.IApprovalOfMedicineController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IApprovalOfMedicineController
   {
      void DisapproveMedicine(Model.ManagerModel.Medicine medicine);
      void ApproveMedicine(Model.ManagerModel.Medicine medicine);
      List<Medicine> SearchMedicine(String query);
   }
}