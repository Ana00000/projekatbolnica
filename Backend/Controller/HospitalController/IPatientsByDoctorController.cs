/***********************************************************************
 * Module:  IPatientsByDoctorController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.HospitalController.IPatientsByDoctorController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IPatientsByDoctorController : IController
   {
      void AddMedicalRecord(Patient patient, Model.PatientModel.MedicalRecord record);
      Model.DoctorModel.DoctorReport GenerateReport(Patient patient, Model.UtilityModel.TimePeriod timePeriod);
      void ReferToSpecialist(Patient patient, Model.DoctorModel.Specialist specialist);
      List<Patient> SearchPatients(String query);
   }
}