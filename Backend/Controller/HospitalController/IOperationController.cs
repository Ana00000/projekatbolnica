/***********************************************************************
 * Module:  IOperationController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.HospitalController.IOperationController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public interface IOperationController : IController
   {
      int CountOperationsInRoom(Model.ManagerModel.Room room, Model.UtilityModel.TimePeriod timePeriod);
   }
}