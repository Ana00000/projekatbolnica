/***********************************************************************
 * Module:  StationaryTreatmentController.cs
 * Author:  Asus
 * Purpose: Definition of the Class Controller.HospitalController.StationaryTreatmentController
 ***********************************************************************/

using System;

namespace Controller.HospitalController
{
   public class StationaryTreatmentController : IStationaryTreatmentController
   {
      public void HospitalizePatient(Patient patient, Model.ManagerModel.Bed bed)
      {
         // TODO: implement
      }
      
      public void DischargePatient(Patient patient)
      {
         // TODO: implement
      }
      
      public void RelocatePatient(Patient patient, Model.ManagerModel.Bed bed)
      {
         // TODO: implement
      }
      
      public Model.UtilityModel.TimePeriod DurationOfLayingInBed(Model.ManagerModel.Bed bed)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.HospitalServices.IStationaryTreatmentService iStationaryTreatmentService;
   
   }
}