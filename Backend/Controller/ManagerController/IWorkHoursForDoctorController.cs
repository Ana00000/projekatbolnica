/***********************************************************************
 * Module:  IWorkHoursForDoctorController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.ManagerController.IWorkHoursForDoctorController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public interface IWorkHoursForDoctorController : Controller.HospitalController.IController
   {
      int CalculateNumberOfDoctorsOnDuty(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours);
      int CalculateNumberOfDoctorsOnVacation(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours);
      Double CalculateOvertimeWorkInPercantage(int overtimeAnnualWork, int overtimeWeeklyWork);
      List<Doctor> OccupationOfDoctors(Model.UtilityModel.TimePeriod from, Model.UtilityModel.TimePeriod to, List<Doctor> doctors);
   }
}