/***********************************************************************
 * Module:  IMedicineController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.ManagerController.IMedicineController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public interface IMedicineController : Controller.HospitalController.IController
   {
      void SendApprovalRequest(List<Doctor> doctors);
   }
}