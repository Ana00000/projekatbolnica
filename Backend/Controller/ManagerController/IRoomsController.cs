/***********************************************************************
 * Module:  IRoomsController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.ManagerController.IRoomsController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public interface IRoomsController : Controller.HospitalController.IController
   {
      int CountNumberOfBedsInRoom(Model.ManagerModel.Room room);
      Model.ManagerModel.RenovationOfRooms AnnounceAction(Model.UtilityModel.TimePeriod timePeriod, Model.ManagerModel.Room room);
      Model.ManagerModel.Room RelocateResources(List<Resource> resources, Model.ManagerModel.Room room);
      Model.ManagerModel.FunctionOfRoom ChangeFunctionOfRoom(Model.ManagerModel.FunctionOfRoom functionOfRoom);
      String ChangeNameOfRoom(String nameOfRoom);
   }
}