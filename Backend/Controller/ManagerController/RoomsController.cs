/***********************************************************************
 * Module:  RoomsController.cs
 * Author:  Asus
 * Purpose: Definition of the Class Controller.ManagerController.RoomsController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public class RoomsController : IRoomsController
   {
      public int CountNumberOfBedsInRoom(Model.ManagerModel.Room room)
      {
         // TODO: implement
         return 0;
      }
      
      public Model.ManagerModel.RenovationOfRooms AnnounceAction(Model.UtilityModel.TimePeriod timePeriod, Model.ManagerModel.Room room)
      {
         // TODO: implement
         return null;
      }
      
      public Model.ManagerModel.Room RelocateResources(List<Resource> resources, Model.ManagerModel.Room room)
      {
         // TODO: implement
         return null;
      }
      
      public Model.ManagerModel.FunctionOfRoom ChangeFunctionOfRoom(Model.ManagerModel.FunctionOfRoom functionOfRoom)
      {
         // TODO: implement
         return null;
      }
      
      public String ChangeNameOfRoom(String nameOfRoom)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.ManagerService.IRoomService iRoomService;
      public Service.ManagerService.IRenovationOfRoomsService iRenovationOfRoomsService;
   
   }
}