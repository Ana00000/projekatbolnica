/***********************************************************************
 * Module:  WorkHoursForDoctorController.cs
 * Author:  Asus
 * Purpose: Definition of the Class Controller.ManagerController.WorkHoursForDoctorController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public class WorkHoursForDoctorController : IWorkHoursForDoctorController
   {
      public int CalculateNumberOfDoctorsOnDuty(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours)
      {
         // TODO: implement
         return 0;
      }
      
      public int CalculateNumberOfDoctorsOnVacation(Model.UtilityModel.TimePeriod timePeriod, List<WorkHoursForDoctor> workHours)
      {
         // TODO: implement
         return 0;
      }
      
      public Double CalculateOvertimeWorkInPercantage(int overtimeAnnualWork, int overtimeWeeklyWork)
      {
         // TODO: implement
         return null;
      }
      
      public List<Doctor> OccupationOfDoctors(Model.UtilityModel.TimePeriod from, Model.UtilityModel.TimePeriod to, List<Doctor> doctors)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.ManagerService.IWorkHoursForDoctorService iWorkHoursForDoctorService;
   
   }
}