/***********************************************************************
 * Module:  IResourceController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.ManagerController.IResourceController
 ***********************************************************************/

using System;

namespace Controller.ManagerController
{
   public interface IResourceController : Controller.HospitalController.IController
   {
      void IncreaseQuantity(String id, int quantity);
      void DecreaseQuantity(String id, int quantity);
      int GetAmountOfResource(int id);
   }
}