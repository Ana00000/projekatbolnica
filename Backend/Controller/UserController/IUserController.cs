/***********************************************************************
 * Module:  IUserController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.UserController.IUserController
 ***********************************************************************/

using System;

namespace Controller.UserController
{
   public interface IUserController : Controller.HospitalController.IController
   {
      String AddFeedback(String message);
      Model.UserModel.Language ChangeLanguage(Model.UserModel.Language language);
      void Registration(User user);
      void Login(String username, String password, Boolean stayLoggedIn);
   }
}