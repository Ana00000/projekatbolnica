/***********************************************************************
 * Module:  IPatientController.cs
 * Author:  Asus
 * Purpose: Definition of the Interface Controller.PatientController.IPatientController
 ***********************************************************************/

using System;

namespace Controller.PatientController
{
   public interface IPatientController : Controller.HospitalController.IController
   {
      Model.DoctorModel.Appointment AddAppointment(Model.DoctorModel.Appointment appointment);
      Model.DoctorModel.Appointment EditAppointment(Model.DoctorModel.Appointment appointment);
      Model.DoctorModel.Appointment CancelAppointment(Model.DoctorModel.Appointment appointment);
      List<Appointment> ViewAppointments(DateTime beginDate, DateTime endDate);
      List<MedicalRecord> ViewRecords(DateTime beginDate, DateTime endDate);
      Model.PatientModel.Survey StartSurvey(Model.PatientModel.Survey survey);
      Model.PatientModel.MedicalRecord AddMedicalRecord(Model.PatientModel.MedicalRecord medicalRecord);
   }
}