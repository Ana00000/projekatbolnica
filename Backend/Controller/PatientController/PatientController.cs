/***********************************************************************
 * Module:  PatientController.cs
 * Author:  Asus
 * Purpose: Definition of the Class Controller.PatientController.PatientController
 ***********************************************************************/

using System;

namespace Controller.PatientController
{
   public class PatientController : IPatientController
   {
      public Model.DoctorModel.Appointment AddAppointment(Model.DoctorModel.Appointment appointment)
      {
         // TODO: implement
         return null;
      }
      
      public Model.DoctorModel.Appointment EditAppointment(Model.DoctorModel.Appointment appointment)
      {
         // TODO: implement
         return null;
      }
      
      public Model.DoctorModel.Appointment CancelAppointment(Model.DoctorModel.Appointment appointment)
      {
         // TODO: implement
         return null;
      }
      
      public List<Appointment> ViewAppointments(DateTime beginDate, DateTime endDate)
      {
         // TODO: implement
         return null;
      }
      
      public List<MedicalRecord> ViewRecords(DateTime beginDate, DateTime endDate)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey StartSurvey(Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.MedicalRecord AddMedicalRecord(Model.PatientModel.MedicalRecord medicalRecord)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.PatientServices.IPatientService iPatientService;
   
   }
}