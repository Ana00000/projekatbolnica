/***********************************************************************
 * Module:  SurveyController.cs
 * Author:  sladj
 * Purpose: Definition of the Class Controller.PatientController.SurveyController
 ***********************************************************************/

using System;

namespace Controller.PatientController
{
   public class SurveyController : ISurveyController
   {
      public Model.PatientModel.Survey MakeSurvery(List<QuestionsAndAnswers> surveryQuestion, string surveryName, string description)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey ChangeSurveryDescription(string description, Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public Model.PatientModel.Survey ChangeSurveryName(string newName, Model.PatientModel.Survey survey)
      {
         // TODO: implement
         return null;
      }
      
      public void Set(Object obj)
      {
         // TODO: implement
      }
      
      public Object Get(Object obj)
      {
         // TODO: implement
         return null;
      }
      
      public List<Object> GetAll()
      {
         // TODO: implement
         return null;
      }
      
      public void New(Object obj)
      {
         // TODO: implement
      }
      
      public void Delete(Object obj)
      {
         // TODO: implement
      }
   
      public Service.PatientServices.ISurveyService iSurveyService;
   
   }
}