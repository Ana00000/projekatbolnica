/***********************************************************************
 * Module:  ISurveyController.cs
 * Author:  sladj
 * Purpose: Definition of the Interface Controller.PatientController.ISurveyController
 ***********************************************************************/

using System;

namespace Controller.PatientController
{
   public interface ISurveyController : Controller.HospitalController.IController
   {
      Model.PatientModel.Survey MakeSurvery(List<QuestionsAndAnswers> surveryQuestion, string surveryName, string description);
      Model.PatientModel.Survey ChangeSurveryDescription(string description, Model.PatientModel.Survey survey);
      Model.PatientModel.Survey ChangeSurveryName(string newName, Model.PatientModel.Survey survey);
   }
}