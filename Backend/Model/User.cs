/***********************************************************************
 * Module:  User.cs
 * Author:  sladj
 * Purpose: Definition of the Class User
 ***********************************************************************/

using System;

namespace Model
{
   public abstract class User
   {
      public Address address;
      public Language language;
   
      private String Username;
      private Boolean Active;
      private Model.UserModel.TypeOfUser Role;
      private String Password;
      private String Name;
      private String Surname;
      private String Email;
      private String PersonalIDnumber;
      private String Telephone;
      private Model.UserModel.Gender Gender;
      private System.DateTime DateOfBirth;
   
   }
}