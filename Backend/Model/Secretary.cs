/***********************************************************************
 * Module:  Secretary.cs
 * Author:  Jelena
 * Purpose: Definition of the Class Secretary
 ***********************************************************************/

using System;

namespace Model
{
   public class Secretary : User
   {
      public System.Collections.ArrayList appointment;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetAppointment()
      {
         if (appointment == null)
            appointment = new System.Collections.ArrayList();
         return appointment;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetAppointment(System.Collections.ArrayList newAppointment)
      {
         RemoveAllAppointment();
         foreach (Model.DoctorModel.Appointment oAppointment in newAppointment)
            AddAppointment(oAppointment);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddAppointment(Model.DoctorModel.Appointment newAppointment)
      {
         if (newAppointment == null)
            return;
         if (this.appointment == null)
            this.appointment = new System.Collections.ArrayList();
         if (!this.appointment.Contains(newAppointment))
            this.appointment.Add(newAppointment);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveAppointment(Model.DoctorModel.Appointment oldAppointment)
      {
         if (oldAppointment == null)
            return;
         if (this.appointment != null)
            if (this.appointment.Contains(oldAppointment))
               this.appointment.Remove(oldAppointment);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllAppointment()
      {
         if (appointment != null)
            appointment.Clear();
      }
      public System.Collections.ArrayList operation;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetOperation()
      {
         if (operation == null)
            operation = new System.Collections.ArrayList();
         return operation;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetOperation(System.Collections.ArrayList newOperation)
      {
         RemoveAllOperation();
         foreach (Model.DoctorModel.Operation oOperation in newOperation)
            AddOperation(oOperation);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddOperation(Model.DoctorModel.Operation newOperation)
      {
         if (newOperation == null)
            return;
         if (this.operation == null)
            this.operation = new System.Collections.ArrayList();
         if (!this.operation.Contains(newOperation))
            this.operation.Add(newOperation);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveOperation(Model.DoctorModel.Operation oldOperation)
      {
         if (oldOperation == null)
            return;
         if (this.operation != null)
            if (this.operation.Contains(oldOperation))
               this.operation.Remove(oldOperation);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllOperation()
      {
         if (operation != null)
            operation.Clear();
      }
      public System.Collections.ArrayList stationaryTreatmentReferral;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetStationaryTreatmentReferral()
      {
         if (stationaryTreatmentReferral == null)
            stationaryTreatmentReferral = new System.Collections.ArrayList();
         return stationaryTreatmentReferral;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetStationaryTreatmentReferral(System.Collections.ArrayList newStationaryTreatmentReferral)
      {
         RemoveAllStationaryTreatmentReferral();
         foreach (Model.DoctorModel.StationaryTreatmentReferral oStationaryTreatmentReferral in newStationaryTreatmentReferral)
            AddStationaryTreatmentReferral(oStationaryTreatmentReferral);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddStationaryTreatmentReferral(Model.DoctorModel.StationaryTreatmentReferral newStationaryTreatmentReferral)
      {
         if (newStationaryTreatmentReferral == null)
            return;
         if (this.stationaryTreatmentReferral == null)
            this.stationaryTreatmentReferral = new System.Collections.ArrayList();
         if (!this.stationaryTreatmentReferral.Contains(newStationaryTreatmentReferral))
            this.stationaryTreatmentReferral.Add(newStationaryTreatmentReferral);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveStationaryTreatmentReferral(Model.DoctorModel.StationaryTreatmentReferral oldStationaryTreatmentReferral)
      {
         if (oldStationaryTreatmentReferral == null)
            return;
         if (this.stationaryTreatmentReferral != null)
            if (this.stationaryTreatmentReferral.Contains(oldStationaryTreatmentReferral))
               this.stationaryTreatmentReferral.Remove(oldStationaryTreatmentReferral);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllStationaryTreatmentReferral()
      {
         if (stationaryTreatmentReferral != null)
            stationaryTreatmentReferral.Clear();
      }
      public System.Collections.ArrayList patient;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetPatient()
      {
         if (patient == null)
            patient = new System.Collections.ArrayList();
         return patient;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetPatient(System.Collections.ArrayList newPatient)
      {
         RemoveAllPatient();
         foreach (Patient oPatient in newPatient)
            AddPatient(oPatient);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddPatient(Patient newPatient)
      {
         if (newPatient == null)
            return;
         if (this.patient == null)
            this.patient = new System.Collections.ArrayList();
         if (!this.patient.Contains(newPatient))
            this.patient.Add(newPatient);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemovePatient(Patient oldPatient)
      {
         if (oldPatient == null)
            return;
         if (this.patient != null)
            if (this.patient.Contains(oldPatient))
               this.patient.Remove(oldPatient);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllPatient()
      {
         if (patient != null)
            patient.Clear();
      }
   
   }
}