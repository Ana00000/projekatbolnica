/***********************************************************************
 * Module:  Room.cs
 * Author:  Jelena
 * Purpose: Definition of the Class Model.ManagerModel.Room
 ***********************************************************************/

using System;

namespace Model.ManagerModel
{
   public class Room
   {
      public System.Collections.ArrayList renovationOfRooms;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetRenovationOfRooms()
      {
         if (renovationOfRooms == null)
            renovationOfRooms = new System.Collections.ArrayList();
         return renovationOfRooms;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetRenovationOfRooms(System.Collections.ArrayList newRenovationOfRooms)
      {
         RemoveAllRenovationOfRooms();
         foreach (RenovationOfRooms oRenovationOfRooms in newRenovationOfRooms)
            AddRenovationOfRooms(oRenovationOfRooms);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddRenovationOfRooms(RenovationOfRooms newRenovationOfRooms)
      {
         if (newRenovationOfRooms == null)
            return;
         if (this.renovationOfRooms == null)
            this.renovationOfRooms = new System.Collections.ArrayList();
         if (!this.renovationOfRooms.Contains(newRenovationOfRooms))
            this.renovationOfRooms.Add(newRenovationOfRooms);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveRenovationOfRooms(RenovationOfRooms oldRenovationOfRooms)
      {
         if (oldRenovationOfRooms == null)
            return;
         if (this.renovationOfRooms != null)
            if (this.renovationOfRooms.Contains(oldRenovationOfRooms))
               this.renovationOfRooms.Remove(oldRenovationOfRooms);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllRenovationOfRooms()
      {
         if (renovationOfRooms != null)
            renovationOfRooms.Clear();
      }
   
      private String NameOfRoom;
      private FunctionOfRoom FunctionOfRoom;
   
   }
}