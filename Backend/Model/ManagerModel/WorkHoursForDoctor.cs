/***********************************************************************
 * Module:  WorkHoursForDoctor.cs
 * Author:  Asus
 * Purpose: Definition of the Class Model.ManagerModel.WorkHoursForDoctor
 ***********************************************************************/

using System;

namespace Model.ManagerModel
{
   public class WorkHoursForDoctor
   {
      public Doctor doctor;
   
      private List<TimePeriod> Shift;
      private int DaysForVacation;
      private int OvertimeAnnualWork;
      private int OvertimeWeeklyWork;
   
   }
}