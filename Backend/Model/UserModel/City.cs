/***********************************************************************
 * Module:  City.cs
 * Author:  Jelena
 * Purpose: Definition of the Class Model.UserModel.City
 ***********************************************************************/

using System;

namespace Model.UserModel
{
   public class City
   {
      public Country country;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public Country GetCountry()
      {
         return country;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newCountry</param>
      public void SetCountry(Country newCountry)
      {
         if (this.country != newCountry)
         {
            if (this.country != null)
            {
               Country oldCountry = this.country;
               this.country = null;
               oldCountry.RemoveCity(this);
            }
            if (newCountry != null)
            {
               this.country = newCountry;
               this.country.AddCity(this);
            }
         }
      }
   
      private String Name;
      private int PostalCode;
   
   }
}