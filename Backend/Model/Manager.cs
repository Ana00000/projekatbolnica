/***********************************************************************
 * Module:  Manager.cs
 * Author:  Asus
 * Purpose: Definition of the Class Manager
 ***********************************************************************/

using System;

namespace Model
{
   public class Manager : User
   {
      public System.Collections.ArrayList workHoursForDoctor;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetWorkHoursForDoctor()
      {
         if (workHoursForDoctor == null)
            workHoursForDoctor = new System.Collections.ArrayList();
         return workHoursForDoctor;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetWorkHoursForDoctor(System.Collections.ArrayList newWorkHoursForDoctor)
      {
         RemoveAllWorkHoursForDoctor();
         foreach (WorkHoursForDoctor oWorkHoursForDoctor in newWorkHoursForDoctor)
            AddWorkHoursForDoctor(oWorkHoursForDoctor);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddWorkHoursForDoctor(WorkHoursForDoctor newWorkHoursForDoctor)
      {
         if (newWorkHoursForDoctor == null)
            return;
         if (this.workHoursForDoctor == null)
            this.workHoursForDoctor = new System.Collections.ArrayList();
         if (!this.workHoursForDoctor.Contains(newWorkHoursForDoctor))
            this.workHoursForDoctor.Add(newWorkHoursForDoctor);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveWorkHoursForDoctor(WorkHoursForDoctor oldWorkHoursForDoctor)
      {
         if (oldWorkHoursForDoctor == null)
            return;
         if (this.workHoursForDoctor != null)
            if (this.workHoursForDoctor.Contains(oldWorkHoursForDoctor))
               this.workHoursForDoctor.Remove(oldWorkHoursForDoctor);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllWorkHoursForDoctor()
      {
         if (workHoursForDoctor != null)
            workHoursForDoctor.Clear();
      }
      public System.Collections.ArrayList renovationOfRooms;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetRenovationOfRooms()
      {
         if (renovationOfRooms == null)
            renovationOfRooms = new System.Collections.ArrayList();
         return renovationOfRooms;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetRenovationOfRooms(System.Collections.ArrayList newRenovationOfRooms)
      {
         RemoveAllRenovationOfRooms();
         foreach (RenovationOfRooms oRenovationOfRooms in newRenovationOfRooms)
            AddRenovationOfRooms(oRenovationOfRooms);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddRenovationOfRooms(RenovationOfRooms newRenovationOfRooms)
      {
         if (newRenovationOfRooms == null)
            return;
         if (this.renovationOfRooms == null)
            this.renovationOfRooms = new System.Collections.ArrayList();
         if (!this.renovationOfRooms.Contains(newRenovationOfRooms))
            this.renovationOfRooms.Add(newRenovationOfRooms);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveRenovationOfRooms(RenovationOfRooms oldRenovationOfRooms)
      {
         if (oldRenovationOfRooms == null)
            return;
         if (this.renovationOfRooms != null)
            if (this.renovationOfRooms.Contains(oldRenovationOfRooms))
               this.renovationOfRooms.Remove(oldRenovationOfRooms);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllRenovationOfRooms()
      {
         if (renovationOfRooms != null)
            renovationOfRooms.Clear();
      }
      public System.Collections.ArrayList resource;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetResource()
      {
         if (resource == null)
            resource = new System.Collections.ArrayList();
         return resource;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetResource(System.Collections.ArrayList newResource)
      {
         RemoveAllResource();
         foreach (Resource oResource in newResource)
            AddResource(oResource);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddResource(Resource newResource)
      {
         if (newResource == null)
            return;
         if (this.resource == null)
            this.resource = new System.Collections.ArrayList();
         if (!this.resource.Contains(newResource))
            this.resource.Add(newResource);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveResource(Resource oldResource)
      {
         if (oldResource == null)
            return;
         if (this.resource != null)
            if (this.resource.Contains(oldResource))
               this.resource.Remove(oldResource);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllResource()
      {
         if (resource != null)
            resource.Clear();
      }
      public System.Collections.ArrayList room;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetRoom()
      {
         if (room == null)
            room = new System.Collections.ArrayList();
         return room;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetRoom(System.Collections.ArrayList newRoom)
      {
         RemoveAllRoom();
         foreach (Room oRoom in newRoom)
            AddRoom(oRoom);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddRoom(Room newRoom)
      {
         if (newRoom == null)
            return;
         if (this.room == null)
            this.room = new System.Collections.ArrayList();
         if (!this.room.Contains(newRoom))
            this.room.Add(newRoom);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveRoom(Room oldRoom)
      {
         if (oldRoom == null)
            return;
         if (this.room != null)
            if (this.room.Contains(oldRoom))
               this.room.Remove(oldRoom);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllRoom()
      {
         if (room != null)
            room.Clear();
      }
   
   }
}