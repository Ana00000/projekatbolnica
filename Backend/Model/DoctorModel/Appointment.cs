/***********************************************************************
 * Module:  Appointment.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.Appointment
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class Appointment
   {
      public Patient patient;
      public Model.Doctor doctor;
      public Model.ManagerModel.Room room;
   
      private String Id;
      private DateTime DateAndTime;
      private TimeSpan Duration;
   
   }
}