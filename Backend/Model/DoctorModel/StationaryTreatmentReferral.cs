/***********************************************************************
 * Module:  StationaryTreatmentReferral.cs
 * Author:  Asus
 * Purpose: Definition of the Class Model.DoctorModel.StationaryTreatmentReferral
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class StationaryTreatmentReferral : Referral
   {
      public Model.Doctor doctor;
      public Patient patient;
   
      private Model.ManagerModel.Bed Bed;
   
   }
}