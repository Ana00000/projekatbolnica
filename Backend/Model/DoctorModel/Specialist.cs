/***********************************************************************
 * Module:  Specialist.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.Specialist
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class Specialist : Model.Doctor
   {
      public Specialty specialty;
      public Operation[] operation;
   
   }
}