/***********************************************************************
 * Module:  SpecialistReferral.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.SpecialistReferral
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class SpecialistReferral : Referral
   {
      public Patient patient;
      public Model.Doctor doctor;
      public Specialist specialist;
   
   }
}