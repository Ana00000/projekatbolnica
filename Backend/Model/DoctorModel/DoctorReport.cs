/***********************************************************************
 * Module:  DoctorReport.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.DoctorReport
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class DoctorReport : Report
   {
      public Model.Doctor doctor;
      public Patient patient;
   
      private Model.UtilityModel.TimePeriod Period;
   
   }
}