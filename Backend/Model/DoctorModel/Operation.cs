/***********************************************************************
 * Module:  Operation.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.Operation
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class Operation
   {
      public Specialist specialist;
      public Patient patient;
      public Model.ManagerModel.Room room;
   
      private String Id;
      private DateTime DateAndTime;
      private TimeSpan Duration;
   
   }
}