/***********************************************************************
 * Module:  Perscription.cs
 * Author:  Asus
 * Purpose: Definition of the Class Doctor.Perscription
 ***********************************************************************/

using System;

namespace Model.DoctorModel
{
   public class Perscription
   {
      public System.Collections.ArrayList medicine;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetMedicine()
      {
         if (medicine == null)
            medicine = new System.Collections.ArrayList();
         return medicine;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetMedicine(System.Collections.ArrayList newMedicine)
      {
         RemoveAllMedicine();
         foreach (Model.ManagerModel.Medicine oMedicine in newMedicine)
            AddMedicine(oMedicine);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddMedicine(Model.ManagerModel.Medicine newMedicine)
      {
         if (newMedicine == null)
            return;
         if (this.medicine == null)
            this.medicine = new System.Collections.ArrayList();
         if (!this.medicine.Contains(newMedicine))
            this.medicine.Add(newMedicine);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveMedicine(Model.ManagerModel.Medicine oldMedicine)
      {
         if (oldMedicine == null)
            return;
         if (this.medicine != null)
            if (this.medicine.Contains(oldMedicine))
               this.medicine.Remove(oldMedicine);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllMedicine()
      {
         if (medicine != null)
            medicine.Clear();
      }
   
      private Model.UtilityModel.TimePeriod PeriodOfValidity;
   
   }
}