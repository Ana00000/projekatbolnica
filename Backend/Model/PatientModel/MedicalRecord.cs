/***********************************************************************
 * Module:  MedicalRecord.cs
 * Author:  Asus
 * Purpose: Definition of the Class Model.PatientModel.MedicalRecord
 ***********************************************************************/

using System;

namespace Model.PatientModel
{
   public class MedicalRecord
   {
      public Model.DoctorModel.Perscription perscription;
      public Doctor doctor;
   
      private DateTime ExamDate;
      private String ExamReport;
   
   }
}